// Copyright - SuperGoonGames LLC 2020 - Not for use by other parties

#pragma once

#include "CoreMinimal.h"
#include "BattleMenuWidgetBase.h"
#include "Blueprint/UserWidget.h"
#include "BattleCommandsWidget.generated.h"

class UBattleMenuButtonBase;
class UScrollBox;
/**
 * This is the UI display that comes up when it is your turn in the battle initially
 */
UCLASS()
class ESCAPETHEFATE_API UBattleCommandsWidget : public UBattleMenuWidgetBase
{
    GENERATED_BODY()
    //Private Variables

    UPROPERTY(BlueprintReadWrite,meta= (AllowPrivateAccess = "true"))
    int32 _currentSelection;
    UPROPERTY(BlueprintReadWrite,meta= (AllowPrivateAccess = "true"))
    int32 _lastSelection;
    UPROPERTY(BlueprintReadWrite,meta= (AllowPrivateAccess = "true"))
    int32 _currentVisibleSelection;
    UPROPERTY(BlueprintReadWrite,meta= (AllowPrivateAccess = "true"))
    int32 _lastVisibleSelection;
    UPROPERTY(BlueprintReadWrite, meta= (AllowPrivateAccess = "true"))
    TArray<UBattleMenuButtonBase*> _currentButtons;
    UPROPERTY(BlueprintReadWrite, meta= (AllowPrivateAccess = "true"))
    bool _isLoading;
    UPROPERTY(BlueprintReadWrite, meta= (AllowPrivateAccess = "true"))
    USoundBase* _cursorSound;

    UPROPERTY(BlueprintReadWrite,meta= (BindWidget, AllowPrivateAccess = "true"))
    TArray<UBattleMenuButtonBase*> _fullButtonArray;
    UPROPERTY(BlueprintReadWrite,meta= (BindWidget, AllowPrivateAccess = "true"))
    TMap<ECharacterMainButtonsAvailable,FText> _skillToTextDict;
    UPROPERTY(BlueprintReadWrite,meta= (BindWidgetAnim, AllowPrivateAccess = "true"))
    UWidgetAnimation* _playerStartTurn;
    UPROPERTY(BlueprintReadWrite,meta= (BindWidgetOptional, AllowPrivateAccess = "true"))
    UScrollBox* _buttonScrollBox;



public:

    virtual void OpenWidget_Implementation(EUiWidgetStates widgetComingFrom) override;
    virtual void SetAllButtonsInactive_Implementation() override;
    virtual void SetButtonActive_Implementation(int32 buttonToSelect) override;
    virtual void UpdateAllButtons_Implementation(int32 buttonToSelect) override;


    /**
    * @brief Decrements the current selection in the menu, probably means it's moving up.
    */
    UFUNCTION(BlueprintCallable)
    void DecrementSelection();

    /**
    * @brief Increments the current selection in the menu, probably means it's moving down.
    */
    UFUNCTION(BlueprintCallable)
    void IncrementSelection();

    virtual void NativeConstruct() override;
};
