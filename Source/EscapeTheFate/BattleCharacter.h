// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BattleCharacterSpawnerComponent.h"
#include "Components/BoxComponent.h"
#include "GameFramework/Pawn.h"
#include "BattleCharacter.generated.h"

enum class EBattleCharacterToSpawn : unsigned char;
class UCharacterStats;
class ABattleCharacter;

//The Battle Character that will be used to create blueprints from.
UCLASS()
class ESCAPETHEFATE_API ABattleCharacter : public APawn
{
    GENERATED_BODY()

public:
    ABattleCharacter();

protected:
    virtual void BeginPlay() override;

private:
    UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category="Graphics",meta = (AllowPrivateAccess = "true"))
    USkeletalMeshComponent* skeletalMesh;

    UPROPERTY(VisibleAnywhere)
    FCharStats characterStats;

public:
    UFUNCTION(BlueprintCallable)
    int32 GetCharacterHealth() const;

    UFUNCTION(BlueprintCallable)
    FString GetCharacterName() const;

    UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category="Sizing")
    UBoxComponent* boxComponent;

    // FCharStats* GetCharacterStats();

    /**
     * @brief This is used when setting the player stats on the start of a battle.  Should only be called on the player spawning to set their stats.
     * @param statsToPutIn The Character stats that are saved in the gameinstance from the players menu
     */
    void SetCharacterStats(FCharStats statsToPutIn)
    {
        characterStats = statsToPutIn;
    }


    UFUNCTION(BlueprintCallable)
    TArray<ECharacterMainButtonsAvailable>CharacterMainButtonsAvailable();
    
    UFUNCTION(BlueprintCallable)
    void DecrementHealth(int damage);

};
