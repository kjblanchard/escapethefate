// Copyright - SuperGoonGames LLC 2020 - Not for use by other parties

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "BattlePlayerPawn.generated.h"

class AEtfHud;
class ABattlePlayerController;
UCLASS()
class ESCAPETHEFATE_API ABattlePlayerPawn : public APawn
{
    GENERATED_BODY()

    UPROPERTY()
    AEtfHud* _etfHud;

    void UpButton();
    void DownButton();
    void SelectButton();
    void CancelButton();

public:
    ABattlePlayerPawn();

    UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Battle")
    ABattlePlayerController* battlePlayerController;


protected:
    virtual void BeginPlay() override;

public:

    virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

    //getters and setters
public:
    UFUNCTION(BlueprintCallable)
    void GetBattleController();

    void SetEtfHud(AEtfHud* etfHud_)
    {
        _etfHud = etfHud_;
    }
};
