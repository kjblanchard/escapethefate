// Fill out your copyright notice in the Description page of Project Settings.


#include "BattleCharacter.h"

// Sets default values
ABattleCharacter::ABattleCharacter()
{
    skeletalMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh"));
    RootComponent = skeletalMesh;
    boxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxComponent"));
    boxComponent->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void ABattleCharacter::BeginPlay()
{
    Super::BeginPlay();
}

int32 ABattleCharacter::GetCharacterHealth() const
{
    return characterStats.enemyHP;
}

FString ABattleCharacter::GetCharacterName() const
{
    return characterStats.enemyName;
}

TArray<ECharacterMainButtonsAvailable> ABattleCharacter::CharacterMainButtonsAvailable()
{
    return characterStats.menuButtons;
}

void ABattleCharacter::DecrementHealth(int damage)
{
    if(characterStats.enemyHP - damage >0)
    {
    characterStats.enemyHP -= damage;
    }
    else
    {
        characterStats.enemyHP = 0;
    }
}
