// Copyright - SuperGoonGames LLC 2020 - Not for use by other parties

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "BattleTargetCursor.generated.h"

/**
 * 
 */
UCLASS()
class ESCAPETHEFATE_API UBattleTargetCursor : public UUserWidget
{
	GENERATED_BODY()
	
};
