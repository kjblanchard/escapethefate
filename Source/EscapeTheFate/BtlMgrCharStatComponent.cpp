// Copyright - SuperGoonGames LLC 2020 - Not for use by other parties


#include "BtlMgrCharStatComponent.h"
#include "BattleCharacter.h"


// Sets default values for this component's properties
UBtlMgrCharStatComponent::UBtlMgrCharStatComponent()
{
}


// Called when the game starts
void UBtlMgrCharStatComponent::BeginPlay()
{
    Super::BeginPlay();
}

void UBtlMgrCharStatComponent::AddToPlayersList(ABattleCharacter* playerToAdd_)
{
    _playerBattleCharacters.Add(playerToAdd_);
}

void UBtlMgrCharStatComponent::AddToEnemiesList(ABattleCharacter* enemyToAdd_)
{
    _enemyBattleCharacters.Add(enemyToAdd_);
}

void UBtlMgrCharStatComponent::DecrementHealth(int danageGiven, int enemyToAttack)
{
    _enemyBattleCharacters[enemyToAttack]->DecrementHealth(danageGiven);
}
