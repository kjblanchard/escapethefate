// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BattleCharacterSpawnerComponent.h"
#include "GameFramework/Actor.h"
#include "BattleManager.generated.h"


class AEtfHud;
class UBtlMgrCharStatComponent;
class UBattleMgrCharStatsComponent;
class UBattleCharacterSpawnerComponent;
enum class EBattleGroups : unsigned char;
enum class EBattleCharacterToSpawn : unsigned char;
class ABattleCharacter;

UCLASS()
class ESCAPETHEFATE_API ABattleManager : public AActor
{
    GENERATED_BODY()

public:
    ABattleManager();

    virtual void BeginPlay() override;


    //Private Components
private:

    UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category = "Battle Spawner Component",meta = (AllowPrivateAccess =
        "true"))
    UBattleCharacterSpawnerComponent* _battleCharacterSpawnerComponent;

    UPROPERTY(VisibleAnywhere, Category= "Character Stats Component")
    UBtlMgrCharStatComponent* _battleMgrCharStatsComponent;

    UPROPERTY(VisibleAnywhere,Category="HUD")
    AEtfHud* _etfHud;

    /**
    * @brief Makes the player in control of the player pawn.
    * @param playerPawn_ The player pawn that is inside the map that will be used for the player to interact with the UI
    */
    void PossessBattlePlayer(APawn* playerPawn_);

    /**
    * @brief Makes the player in control of the player pawn, and initializes the hud and passes information into the hud so that it can reference some of the information to display it.
    */
    void SetupBattleHud();


public:
    UFUNCTION(BlueprintCallable)
    void StartBattle(const TArray<FBattleGroupToTransform>& playersArray_,
                     const TArray<FBattleGroupToTransform>& enemiesArray_, const TArray<FTransform>& cameraArray_,
                     APawn* playerToPossess_,
                     ACameraActor* cameraToControl_);

};
