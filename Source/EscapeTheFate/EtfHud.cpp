// Copyright - SuperGoonGames LLC 2020 - Not for use by other parties


#include "EtfHud.h"
#include "Blueprint/UserWidget.h"



AEtfHud::AEtfHud()
{
    _bUiVisible = false;
}

void AEtfHud::SetupBattleHud(TArray<ABattleCharacter*> players, TArray<ABattleCharacter*> enemies)
{
    PlayerCharactersStats = players;
    EnemyCharactersStats = enemies;
    CreateBattleHud();

}


