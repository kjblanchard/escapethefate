// Copyright - SuperGoonGames LLC 2020 - Not for use by other parties


#include "BattleCommandsWidget.h"
#include "Components/ScrollBox.h"
#include "Kismet/GameplayStatics.h"
#include "BattleMenuButtonBase.h"
#include "BattleMenuWidgetBase.h"


void UBattleCommandsWidget::OpenWidget_Implementation(EUiWidgetStates widgetComingFrom)
{
}

void UBattleCommandsWidget::SetAllButtonsInactive_Implementation()
{
    for (auto button : _currentButtons)
    {
        button->SetButtonInactive();
    }
}

void UBattleCommandsWidget::SetButtonActive_Implementation(int32 buttonToSelect)
{
    for (int i = 0; i < _currentButtons.Num(); ++i)
    {
        if (buttonToSelect == i)
        {
            auto buttonToChoose = _currentButtons[i];
            buttonToChoose->SetButtonActive();
            if (_buttonScrollBox)
            {
                _buttonScrollBox->ScrollWidgetIntoView(buttonToChoose);
            }
            break;
        }
    }
}

void UBattleCommandsWidget::UpdateAllButtons_Implementation(int32 buttonToSelect)
{
    SetAllButtonsInactive();
    SetButtonActive(buttonToSelect);
}


void UBattleCommandsWidget::DecrementSelection()
{
    _lastSelection = _currentSelection;
    --_currentSelection;
    _currentSelection = FMath::Clamp(_currentSelection, 0, _currentButtons.Num() - 1);
    if (_currentSelection != _lastSelection)
    {
        _lastVisibleSelection = _currentVisibleSelection;
        --_currentVisibleSelection;
        _currentVisibleSelection = FMath::Clamp(_currentVisibleSelection, 0, 2);
        UpdateAllButtons(_currentSelection);
        PlaySfx(SfxEnum::SFX_CursorMove);
    }
}

void UBattleCommandsWidget::IncrementSelection()
{
    _lastSelection = _currentSelection;
    ++_currentSelection;
    _currentSelection = FMath::Clamp(_currentSelection, 0, _currentButtons.Num() - 1);
    if (_currentSelection != _lastSelection)
    {
        FString name = this->GetName();
        UE_LOG(LogTemp, Warning, TEXT( " : %s" ), *name);
        _lastVisibleSelection = _currentVisibleSelection;
        ++_currentVisibleSelection;
        _currentVisibleSelection = FMath::Clamp(_currentVisibleSelection, 0, 2);
        UpdateAllButtons(_currentSelection);
        PlaySfx(SfxEnum::SFX_CursorMove);
    }
}

void UBattleCommandsWidget::NativeConstruct()
{
    _fullBattleHud = GetFullBattleHudReference();
    Super::NativeConstruct();
}
