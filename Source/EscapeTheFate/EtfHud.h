
#pragma once

#include "CoreMinimal.h"


#include "BattleFullHudWidget.h"
#include "BattleMenuWidgetBase.h"
#include "GameFramework/HUD.h"
#include "EtfHud.generated.h"
struct FCharStats;
class ABattleCharacter;
/**
 * 
 */
UCLASS()
class ESCAPETHEFATE_API AEtfHud : public AHUD
{
    GENERATED_BODY()
    UPROPERTY(VisibleAnywhere)
    TArray<ABattleCharacter*> _battleCharacters;


    UPROPERTY(BlueprintReadWrite,meta = (AllowPrivateAccess = "true"))
    bool _bUiVisible;

    //player and enemy stats

    TArray<FCharStats*> PlayerCharacterStats;
    TArray<FCharStats*> EnemyCharacterStats;


public:
    AEtfHud();
    UPROPERTY(BlueprintReadOnly, EditAnywhere)
    TArray<ABattleCharacter*> PlayerCharactersStats;
    UPROPERTY(BlueprintReadOnly,EditAnywhere)
    TArray<ABattleCharacter*> EnemyCharactersStats;

    void SetupBattleHud(TArray<ABattleCharacter*> players, TArray<ABattleCharacter*> enemies);

    UFUNCTION(BlueprintImplementableEvent)
    void CreateBattleHud();

    UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
    void SendButtonInput(EUiButtons buttonPressed_);

    UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
    void ButtonHovered(int32 buttonHoveredOver);

    UPROPERTY(BlueprintReadWrite,meta= (AllowPrivateAccess = "true"))
    UBattleFullHudWidget* _fullBattleHud;
};
