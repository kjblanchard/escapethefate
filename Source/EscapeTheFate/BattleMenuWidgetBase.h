// Copyright - SuperGoonGames LLC 2020 - Not for use by other parties

#pragma once

#include "CoreMinimal.h"


#include "SoundManager.h"

#include "Blueprint/UserWidget.h"
#include "BattleMenuWidgetBase.generated.h"


class UBattleFullHudWidget;
/**
* @brief Current button being pressed for handling input
*/
UENUM(BlueprintType)
enum class EUiButtons : uint8
{
    Up,
    Down,
    Left,
    Right,
    Confirm,
    Cancel,
    Start,
    Select
};

/**
* @brief Used for changing widget states for the full battle UI
*/
UENUM(BlueprintType)
enum class EUiWidgetStates : uint8
{
    DEFAULT,
    Main,
    TargetSelection
};


/**
* @brief The buttons that the character has available, set on the next battle component currently.
*/
UENUM(BlueprintType)
enum class ECharacterMainButtonsAvailable : uint8
{
    DEFAULT,
    Attack,
    Skill,
    Special,
    Black_Magic,
    White_Magic,
    Item,
    Defend
};

class ASoundManager;
/**
 * Depreciated, please remove when converted to UIwidgetstate interface.
 */
UCLASS()
class ESCAPETHEFATE_API UBattleMenuWidgetBase : public UUserWidget
{
    GENERATED_BODY()

protected:
    UPROPERTY(BlueprintReadOnly)
    ASoundManager* _soundManager;

    UPROPERTY(BlueprintReadOnly)
    UBattleFullHudWidget* _fullBattleHud = nullptr;


    //protected Methods


public:

    /**
    * @brief Ran when there is a change of widget state from the full Battle hud, this will take care of playing the animations and resetting things to default if needed
    */
    UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
    void OpenWidget(EUiWidgetStates widgetComingFrom = EUiWidgetStates::DEFAULT);

    /**
    * @brief Ran when there is a change of widget state from the full Battle hud, this will take care of playing the animations and resetting things to default if needed
    */
    UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
    void CloseWidget(EUiWidgetStates widgetGoingTo = EUiWidgetStates::DEFAULT);

    /**
    * @brief This is meant to be overrode to listen to all of the incoming button presses and handle them.
    * @param buttonPressed_ The current button press sent to the Widget
    */
    UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
    void SendButtonInput(EUiButtons buttonPressed_);

    /**
    * @brief This is called by the button when it is clicked in the UI.
    */
    UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
    void ButtonPressed(int32 buttonToSelect);

    /**
    * @brief This is called to highlight the button that is being hovered over by the button press after inactivating all of them.
    * @param buttonToSelect The button that is hovered over
    */
    UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
    void UpdateAllButtons(int32 buttonToSelect);

    /**
    * @brief Sets all the buttons to Inactive (none are highlighted) 
    */
    UFUNCTION(BlueprintNativeEvent,BlueprintCallable)
    void SetAllButtonsInactive();

    /**
    * @brief Activates the current button, usually ran after you inactivate all of the buttons.
    * @param buttonToSelect The button that you want to activate
    */
    UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
    void SetButtonActive(int32 buttonToSelect);


    /**
     * @brief This is used to play sounds from within the widget.  Soundmanager should be gathered at some point during construction of the widget.
     * @param soundToPlay The enum value of the SFX that should be played
     */
    UFUNCTION(BlueprintCallable)
    void PlaySfx(SfxEnum soundToPlay);

    UFUNCTION(BlueprintCallable)
    UBattleFullHudWidget* GetFullBattleHudReference();
};
