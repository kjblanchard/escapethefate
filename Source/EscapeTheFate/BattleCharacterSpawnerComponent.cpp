// Copyright - SuperGoonGames LLC 2020 - Not for use by other parties


#include "BattleCharacterSpawnerComponent.h"
#include "EtfGameInstance.h"
#include "Kismet/GameplayStatics.h"
#include "Camera/CameraActor.h"
#include "BattleManager.h"
#include "BattleCharacter.h"
#include "BtlMgrCharStatComponent.h"

// Sets default values for this component's properties
UBattleCharacterSpawnerComponent::UBattleCharacterSpawnerComponent(): _currentGameInstance(nullptr),
                                                                      _escapeTheFateCharacter(nullptr),
                                                                      _nextBattleGroup(EBattleGroups::DEFAULT),
                                                                      _playersToSpawnStatsArray({}),
                                                                      _playersToSpawnTransforms({}),
                                                                      _enemiesToSpawnArray({}),
                                                                      _enemiesToSpawnTransforms({}),
                                                                      _playerSpawnLocations({}),
                                                                      _enemySpawnLocations({})

{
    _currentGameInstance = Cast<UEtfGameInstance>(UGameplayStatics::GetGameInstance(this));
}


void UBattleCharacterSpawnerComponent::SpawnBattlersAndCamera(const TArray<FBattleGroupToTransform>& playersArray_,
                                                              const TArray<FBattleGroupToTransform>& enemiesArray_,
                                                              const TArray<FTransform>& cameraArray_,
                                                              ACameraActor* cameraToControl_)
{
    const auto playerController = UGameplayStatics::GetPlayerController(this, 0);
    GatherInformationForStartBattle(playersArray_, enemiesArray_);
    SetupBattleCamera(cameraArray_[_battleLocation], cameraToControl_, playerController);
    SpawnAllCharacters();
}

void UBattleCharacterSpawnerComponent::GatherInformationForStartBattle(
    TArray<FBattleGroupToTransform> playersGroupToTransforms_,
    TArray<FBattleGroupToTransform> enemiesGroupToTransforms_)
{
    GatherInformationFromGameInstance();
    _playersToSpawnTransforms = playersGroupToTransforms_;
    _enemiesToSpawnTransforms = enemiesGroupToTransforms_;
    _enemiesToSpawnArray = ConvertBattleGroupToEnemies(_nextBattleGroup);
    _battleLocation = _enemiesToSpawnArray.battleLocation;
    _playerSpawnLocations = _playersToSpawnTransforms[_battleLocation].battleTransforms;
    _enemySpawnLocations = _enemiesToSpawnTransforms[_battleLocation].battleTransforms;
}

FBattleCharactersAndBattleLocation UBattleCharacterSpawnerComponent::ConvertBattleGroupToEnemies(
    const EBattleGroups battleGroups_) const
{
    return _battleGroupToCharacterDictionary.FindRef(battleGroups_);
}

void UBattleCharacterSpawnerComponent::GatherInformationFromGameInstance()
{
    const auto gameInstanceInfo = _currentGameInstance->NextBattleInformation();
    _playersToSpawnStatsArray = gameInstanceInfo.PlayersToSpawn;
    _nextBattleGroup = gameInstanceInfo.BattleGroupToSpawn;
}

void UBattleCharacterSpawnerComponent::SetupBattleCamera(FTransform cameraTransform, ACameraActor* camera,
                                                         APlayerController* playerController_)
{
    camera->SetActorTransform(cameraTransform);
    playerController_->SetViewTargetWithBlend(camera);
}

void UBattleCharacterSpawnerComponent::SpawnAllCharacters()
{
    auto battleManager = _currentGameInstance->BattleManager();
    if (battleManager)
    {
        for (int i = 0; i < _playersToSpawnStatsArray.Num(); ++i)
        {
            const auto newPlayer = SpawnPlayer(_playersToSpawnStatsArray[i].characterToSpawn, _playerSpawnLocations[i]);
            if (newPlayer)
            {
                newPlayer->SetCharacterStats(_playersToSpawnStatsArray[i]);
                if (_btlMgrCharStatComponent)
                {
                    _btlMgrCharStatComponent->AddToPlayersList(newPlayer);
                }
            }
        }

        for (int i = 0; i < _enemiesToSpawnArray.battleCharacter.Num(); ++i)
        {
            const auto newEnemy = SpawnPlayer(_enemiesToSpawnArray.battleCharacter[i], _enemySpawnLocations[i]);
            if (newEnemy)
            {
                if (_btlMgrCharStatComponent)
                {
                    _btlMgrCharStatComponent->AddToEnemiesList(newEnemy);
                }
            }
        }
    }
}
