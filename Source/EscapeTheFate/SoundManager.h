// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"


#include "SoundManager.generated.h"
class USoundCue;

UENUM(BlueprintType)
enum class SoundEnum : uint8
{
    SE_Facility,
    SE_Battle,
    SE_Default UMETA(Hidden)
};

UENUM(BlueprintType)
enum class SfxEnum : uint8
{
    SFX_CursorMove,
    SE_Default UMETA(Hidden)
};

//SoundManager class used in the GameManager-Persistant area
UCLASS()
class ESCAPETHEFATE_API ASoundManager : public AActor
{
    GENERATED_BODY()

public:
    // Sets default values for this actor's properties
    ASoundManager();

protected:
    // Called when the game starts or when spawned
    virtual void BeginPlay() override;


private:
    UPROPERTY(VisibleAnywhere,BlueprintReadOnly,Category="Sounds",meta = (AllowPrivateAccess = "true"))
    TMap<SoundEnum, USoundCue*> _dictionaryForMusic;

    UPROPERTY(VisibleAnywhere,BlueprintReadOnly,Category="Sounds",meta = (AllowPrivateAccess = "true"))
    TMap<SfxEnum, USoundBase*> _dictionaryForSfx;

    UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category="Sounds",meta = (AllowPrivateAccess = "true"))
    TArray<USoundCue*> _bgmMusic;

    UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category="Sounds",meta = (AllowPrivateAccess = "true"))
    TArray<USoundBase*> _sfxSounds;

    UPROPERTY(VisibleAnywhere,BlueprintReadWrite,Category="Sounds",meta = (AllowPrivateAccess = "true"))
    UAudioComponent* _currentPlayingMusic;

    //publically called Methods
public:
    UFUNCTION(BlueprintImplementableEvent,BlueprintCallable)
    void PlayBgm(SoundEnum musicToPlay_);

    UFUNCTION(BlueprintImplementableEvent,BlueprintCallable)
    void StopBgm();

    UFUNCTION(BlueprintImplementableEvent,BlueprintCallable)
    void PlaySfx(SfxEnum musicToPlay_);

    UFUNCTION(BlueprintCallable,BlueprintImplementableEvent)
    void AddBgmToDictionary();

    UFUNCTION(BlueprintCallable,BlueprintImplementableEvent)
    void AddSfxToDictionary();
};
