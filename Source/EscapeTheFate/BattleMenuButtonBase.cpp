// Copyright - SuperGoonGames LLC 2020 - Not for use by other parties


#include "BattleMenuButtonBase.h"



void UBattleMenuButtonBase::InitializeButton(int32 buttonNumber_, UBattleMenuWidgetBase* fullBattleHud_,
                                             FText buttonName_,
                                             ECharacterMainButtonsAvailable buttonType_)
{
    _buttonNumber = buttonNumber_;
    _fullBattleHud = fullBattleHud_;
    _buttonType = buttonType_;
    _buttonName = buttonName_;
    ButtonTxt->Text = _buttonName;
}

int32 UBattleMenuButtonBase::SetButtonActive()
{
    if(_isActive == false)
    {
        _isActive = true;
        SetRenderScale(_buttonSelectedSize);
        IdleCursor->SetVisibility(ESlateVisibility::Visible);
        PlayAnimation(BlinkingTextAnimation,0,0);
        PlayAnimation(IdleCursorAnimation, 0, 0);
    }
    return _buttonNumber;
}

int32 UBattleMenuButtonBase::SetButtonInactive()
{
    if(_isActive)
    {
        _isActive = false;
        SetRenderScale(_buttonRegularSize);
        IdleCursor->SetVisibility(ESlateVisibility::Hidden);
        StopAnimation(BlinkingTextAnimation);
        StopAnimation(IdleCursorAnimation);
    }
    return _buttonNumber;
}

void UBattleMenuButtonBase::DisableCursor()
{
    StopAnimation(IdleCursorAnimation);
}



