// Copyright - SuperGoonGames LLC 2020 - Not for use by other parties


#include "BattleZone.h"

#include "EscapeTheFateCharacter.h"
#include "Components/SphereComponent.h"
#include "BattleCharacterSpawnerComponent.h"



// Sets default values
ABattleZone::ABattleZone()
{
    _sphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("Battle Zone"));
    RootComponent = _sphereComponent;
    _sphereComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
    _sphereComponent->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Overlap);

    _zoneBattleWeightValues = {};
    _highestZoneBattleWeight = 0;
}

// Called when the game starts or when spawned
void ABattleZone::BeginPlay()
{
    Super::BeginPlay();
    GenerateBattleZoneWeights();
    _sphereComponent->OnComponentBeginOverlap.AddDynamic(this, &ABattleZone::OnOverlapBegin);
    _sphereComponent->OnComponentEndOverlap.AddDynamic(this, &ABattleZone::OnOverlapEnd);
}


void ABattleZone::OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
                                 UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep,
                                 const FHitResult& SweepResult)
{
    AEscapeTheFateCharacter* etfChar = Cast<AEscapeTheFateCharacter>(OtherActor);
    if (!etfChar)
    {
        return;
    }
    etfChar->SetNextBattleSteps(_stepsUntilBattle);
    etfChar->SetIsInBattleZone(true);
    etfChar->SetNextBattleZone(GenerateRandomBattleGroupForNextBattle());
}

void ABattleZone::OnOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
                               UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
    AEscapeTheFateCharacter* etfChar = Cast<AEscapeTheFateCharacter>(OtherActor);
    if (!etfChar)
    {
        return;
    }
    etfChar->SetIsInBattleZone(false);
}

void ABattleZone::GenerateBattleZoneWeights()
{
    _numberOfZoneGroups = _zoneBattleGroups.Num();
    for (auto _zoneBattleGroup : _zoneBattleGroups)
    {
        auto valueToAdd = _zoneBattleGroup.weightValue * _numberOfZoneGroups + _highestZoneBattleWeight;
        _zoneBattleWeightValues.Add(valueToAdd);
        _highestZoneBattleWeight = valueToAdd;
    }
}

EBattleGroups ABattleZone::GenerateRandomBattleGroupForNextBattle()
{
    if (_zoneBattleGroups.Num() == 0)
    {
        return EBattleGroups::DEFAULT;
    }

    const auto randomRoll = FMath::RandRange(0, _highestZoneBattleWeight);
    for (int i = 0; i < _zoneBattleGroups.Num(); ++i)
    {
        if (randomRoll <= _zoneBattleWeightValues[i])
        {
            return _zoneBattleGroups[i].battleGroup;
        }
    }
    return EBattleGroups::DEFAULT;
}

