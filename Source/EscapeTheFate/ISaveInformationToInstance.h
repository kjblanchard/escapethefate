// Copyright - SuperGoonGames LLC 2020 - Not for use by other parties

#pragma once

#include "CoreMinimal.h"

class UEtfGameInstance;
/**
 * This class is used to give an interface to save things to the GameInstance.  Make sure to set the gameinstance if this is run inside a component, as they cannot get the gameinstance.
 */
class ESCAPETHEFATE_API ISaveInformationToInstance
{
    virtual void SaveToGameInstance() = 0;
};
