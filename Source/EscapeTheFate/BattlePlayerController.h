// Copyright - SuperGoonGames LLC 2020 - Not for use by other parties

#pragma once

#include "CoreMinimal.h"


#include "GameFramework/PlayerController.h"

#include "BattlePlayerController.generated.h"

class AEtfHud;
/**
 * This class is used for the player to interact with the UI within a battle.
 */
UCLASS()
class ESCAPETHEFATE_API ABattlePlayerController : public APlayerController
{
    GENERATED_BODY()


    virtual void Tick(float DeltaSeconds) override;


    virtual void BeginPlay() override;

// public:
//     UFUNCTION(BlueprintCallable)
//     float GetObjectScreenRadius(AActor* inActor_);
};
