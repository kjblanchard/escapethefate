// Copyright - SuperGoonGames LLC 2020 - Not for use by other parties

#pragma once

#include "CoreMinimal.h"



#include "BattleMenuWidgetBase.h"
#include "Components/ActorComponent.h"
#include "BattleCharacterSpawnerComponent.generated.h"

class UBtlMgrCharStatComponent;
class AEscapeTheFateCharacter;
class UEtfGameInstance;
class ABattleCharacter;


/**
 * @brief Used to map the battle group of the map to the spawn transforms that should be used in battle
 */
USTRUCT(BlueprintType)
struct FBattleGroupToTransform
{
    GENERATED_BODY()
    UPROPERTY(EditDefaultsOnly)
    int32 battleGroupNumber;

    UPROPERTY(EditDefaultsOnly)
    TArray<FTransform> battleTransforms;
};


/**
 * @brief Used for The Spawn Enemy Function and for the Dictionary to map an Enemy to a Blueprint Object.
 */
UENUM(BlueprintType)
enum class EBattleCharacterToSpawn : uint8
{
    Default,
    Kevin,
    Cory,
    Todd,
    Melissa,
    DirtPecker
};

//The Stats for each Battle Character should be Stored in this Enum.
USTRUCT(BlueprintType)
struct FCharStats
{
    GENERATED_USTRUCT_BODY()

    UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category="CharacterStats")
    EBattleCharacterToSpawn characterToSpawn;

    UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category="CharacterStats")
    FString enemyName;

    UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category="CharacterStats")
    int32 enemyHP;

    UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category="CharacterStats")
    TArray<ECharacterMainButtonsAvailable> menuButtons;
};

/**
 * @brief Groups the battle enemies to a battle location?
 */
USTRUCT(BlueprintType)
struct FBattleCharactersAndBattleLocation
{
    GENERATED_BODY()
    UPROPERTY(EditDefaultsOnly)
    int32 battleLocation;

    UPROPERTY(EditDefaultsOnly)
    TArray<EBattleCharacterToSpawn> battleCharacter;
};


/**
 * @brief Used for a list of all the different "Battle groups" that you can encounter in the game.  You'll set up
 * what enemies these spawn inside the spawner component bp dictionary files
 */
UENUM(BlueprintType)
enum class EBattleGroups : uint8
{
    DEFAULT,
    DEBUGBATTLEGROUP1,
    DEBUGBATTLEGROUP2,
    DEBUGBATTLEGROUP3
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent), Blueprintable )
class ESCAPETHEFATE_API UBattleCharacterSpawnerComponent : public UActorComponent
{
    GENERATED_BODY()
    UEtfGameInstance* _currentGameInstance;
    UPROPERTY()
    AEscapeTheFateCharacter* _escapeTheFateCharacter;
    UPROPERTY()
    UBtlMgrCharStatComponent* _btlMgrCharStatComponent;
    UPROPERTY()
    EBattleGroups _nextBattleGroup;
    UPROPERTY()
    TArray<FCharStats> _playersToSpawnStatsArray;
    UPROPERTY()
    TArray<FBattleGroupToTransform> _playersToSpawnTransforms;
    UPROPERTY()
    FBattleCharactersAndBattleLocation _enemiesToSpawnArray;
    UPROPERTY()
    TArray<FBattleGroupToTransform> _enemiesToSpawnTransforms;
    TArray<FTransform> _playerSpawnLocations;
    TArray<FTransform> _enemySpawnLocations;

    UPROPERTY()
    int32 _battleLocation;

    UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,meta = (AllowPrivateAccess = "true"))
    TMap<EBattleCharacterToSpawn, TSubclassOf<ABattleCharacter>> _battleCharacterToCharacterDictionary;

    UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,meta = (AllowPrivateAccess = "true"))
    TMap<EBattleGroups, FBattleCharactersAndBattleLocation> _battleGroupToCharacterDictionary;


    //Private functions
private:
    /**
     * @brief Grabs information from the GameInstance that was put there from the player during the screen transition, it is then used to set the information for the battle and modified as needed.
     */
    void GatherInformationFromGameInstance();

    /**
     * @brief Loops through the list of enemies and players, and spawns them at their locations, and stores the pointers in an array.
     */
    void SpawnAllCharacters();

    /**
     * @brief Converts a enum to a array of enemies and a location. This may be good to put in json in the future.
     * @param battleGroups_ The Battle Group that is gathered from the player character that needs to be converted in BP
     * @return Returns a struct that contains the array of enemies, along with the battle location
     */
    FBattleCharactersAndBattleLocation ConvertBattleGroupToEnemies(EBattleGroups battleGroups_) const;

    /**
    * @brief Converts information from start battle into the local variables
    * 
    */
    void GatherInformationForStartBattle(TArray<FBattleGroupToTransform> playersGroupToTransforms_,
                                         TArray<FBattleGroupToTransform> enemiesGroupToTransforms_);

    /**
     * @brief This sets up the main battle camera that will be used for the battle
     * @param cameraTransform The location for the camera to be at
     * @param camera The camera that is inside of the map that will be used as the main battle camera
     * @param playerController_ The player controller that should be taken over, usually 0
     */
    void SetupBattleCamera(FTransform cameraTransform, ACameraActor* camera, APlayerController* playerController_);


public:
    UBattleCharacterSpawnerComponent();

    /**
     * @brief Takes care of spawning the players and enemies, This should be what is called from the battle manager.
     * @param playersArray_ The players and battle group that will be used to spawn the group
     * @param enemiesArray_ The enemies and battle group that will be used to spawn the group
     * @param cameraArray_ The list of transforms that the camera can spawn in
     * @param cameraToControl_ The reference to the main battle camera that should be in the map
     */
    void SpawnBattlersAndCamera(const TArray<FBattleGroupToTransform>& playersArray_,
                                const TArray<FBattleGroupToTransform>& enemiesArray_,
                                const TArray<FTransform>& cameraArray_,
                                ACameraActor* cameraToControl_);
    /**
    * @brief Used inside the start battle function to spawn the Characters onto the battlefield.  Defined in BP as we need to spawn BattleCharacter BPs which hold the stats/animations
    * @param thingToSpawn Enum value for the character BP to spawn
    * @param locationToSpawn The location to spawn the character BP
    * @return Returns the character spawned for manipulation in the battle
    */
    UFUNCTION(BlueprintCallable,BlueprintImplementableEvent)
    ABattleCharacter* SpawnPlayer(EBattleCharacterToSpawn thingToSpawn, FTransform locationToSpawn);


    //Getters and Setters

    void SetBtlMgrCharStatComponent(UBtlMgrCharStatComponent* btlMgrCharStatComponent_)
    {
        _btlMgrCharStatComponent = btlMgrCharStatComponent_;
    }
};
