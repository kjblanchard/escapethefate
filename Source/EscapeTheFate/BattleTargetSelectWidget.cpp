// Copyright - SuperGoonGames LLC 2020 - Not for use by other parties


#include "BattleTargetSelectWidget.h"

void UBattleTargetSelectWidget::SwitchToEnemies()
{
    if (_isSelectingPlayers)
    {
        _isSelectingEnemies = true;
        _isSelectingPlayers = false;
    }
}

void UBattleTargetSelectWidget::SwitchToPlayers()
{
    if (_isSelectingEnemies)
    {
        _isSelectingPlayers = true;
        _isSelectingEnemies = false;
    }
}

void UBattleTargetSelectWidget::NativeConstruct()
{
    _fullBattleHud = GetFullBattleHudReference();
    Super::NativeConstruct();
}
