// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"



#include "BattleCharacterSpawnerComponent.h"
#include "Engine/GameInstance.h"
#include "EtfGameInstance.generated.h"

enum class EBattleGroups : unsigned char;
class ABattleManager;
class AEscapeTheFateCharacter;
class ASoundManager;
class AGameManager;

/**
 * @brief Used to save the data from the next battle component to the game instance, for loading between screens.
 * Likely going to need to add character stats to this in the future.
 */
USTRUCT(BlueprintType)
struct FNextBattleInformation
{
    GENERATED_BODY()
    TArray<FCharStats> PlayersToSpawn;
    UPROPERTY(BlueprintReadOnly)
    EBattleGroups BattleGroupToSpawn;
};


/**
 * The game instance used in the game.  This will store variables that is taken from level to level.  It is also created into a blueprint and used in the Editor.
 */
UCLASS()
class ESCAPETHEFATE_API UEtfGameInstance : public UGameInstance
{
    GENERATED_BODY()

private:
    UPROPERTY(BlueprintReadOnly,meta = (AllowPrivateAccess =
        "true"))
    ABattleManager* _battleManager;


    UPROPERTY(BlueprintReadOnly,meta = (AllowPrivateAccess =
        "true"))
    ASoundManager* _soundManager;

    UPROPERTY(BlueprintReadOnly,meta = (AllowPrivateAccess =
        "true"))
    AEscapeTheFateCharacter* _escapeTheFateCharacter;

    UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
    FNextBattleInformation _nextBattleInformation;


public:

    //Getters and Setters
    ABattleManager* BattleManager() const
    {
        return _battleManager;
    }

    UFUNCTION(BlueprintCallable)
    void SetBattleManager(ABattleManager* battleManager_)
    {
        _battleManager = battleManager_;
    }

    ASoundManager* SoundManager() const
    {
        return _soundManager;
    }

    UFUNCTION(BlueprintCallable)
    void SetSoundManager(ASoundManager* soundManager_)
    {
        if (!_soundManager)
        {
            _soundManager = soundManager_;
        }
    }

    FNextBattleInformation NextBattleInformation() const
    {
        return _nextBattleInformation;
    }

    void SetNextBattleInformation(FNextBattleInformation battleInformation_)
    {
        _nextBattleInformation = battleInformation_;

    }
};
