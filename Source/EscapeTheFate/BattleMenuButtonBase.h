// Copyright - SuperGoonGames LLC 2020 - Not for use by other parties

#pragma once

#include "CoreMinimal.h"

#include "BattleMenuWidgetBase.h"
#include "Blueprint/UserWidget.h"
#include "Components/Button.h"
#include "Components/Image.h"
#include "Components/TextBlock.h"
#include "BattleMenuButtonBase.generated.h"


/**
 * Base class used for all the button widgets in the battle
 */
UCLASS()
class ESCAPETHEFATE_API UBattleMenuButtonBase : public UUserWidget
{
    GENERATED_BODY()

    UPROPERTY(BlueprintReadOnly,meta = (AllowPrivateAccess = "true"))
    int32 _buttonNumber;

    
    UPROPERTY(meta = (BindWidgetAnim))
    UWidgetAnimation* IdleCursorAnimation;
    UPROPERTY(meta = (BindWidgetAnim))
    UWidgetAnimation* BlinkingTextAnimation;

    UPROPERTY(meta = (BindWidget))
    UImage* IdleCursor;

    UPROPERTY(meta =(BindWidget))
    UTextBlock* ButtonTxt;

    UPROPERTY(meta =(BindWidget))
    UButton* Button;


    UPROPERTY(BlueprintReadOnly,meta = (AllowPrivateAccess = "true"))
    UBattleMenuWidgetBase* _fullBattleHud;
    
    UPROPERTY(BlueprintReadWrite,meta = (AllowPrivateAccess = "true"))
    UTexture2D* _cursorImage;
    ECharacterMainButtonsAvailable _buttonType;
    bool _isActive;
    FText _buttonName;

    UPROPERTY(EditDefaultsOnly, meta = (AllowPrivateAccess = "true"))
    FVector2D _buttonSelectedSize;
    UPROPERTY(EditDefaultsOnly, meta = (AllowPrivateAccess = "true"))
    FVector2D _buttonRegularSize;

    //Private Functions


    //Public Functions
public:


    UFUNCTION(BlueprintCallable)
    void InitializeButton(int32 buttonNumber_, UBattleMenuWidgetBase* fullBattleHud_, FText buttonName_,
                          ECharacterMainButtonsAvailable buttonType_ = ECharacterMainButtonsAvailable::DEFAULT);

    UFUNCTION(BlueprintCallable)
    int32 SetButtonActive();

    UFUNCTION(BlueprintCallable)
    int32 SetButtonInactive();

    UFUNCTION(BlueprintCallable)
    void DisableCursor();

    //Getters and Setters
    UFUNCTION(BlueprintCallable)
    bool IsActive() const
    {
        return _isActive;
    }

    UFUNCTION(BlueprintCallable)
    void SetIsActive(bool isActive_)
    {
        _isActive = isActive_;
    }

    //Probaly Dont need a lot of these once C++ is done..
    UFUNCTION(BlueprintCallable)
    void SetButtonType(ECharacterMainButtonsAvailable buttonType)
    {
        _buttonType = buttonType;
    }
    UFUNCTION(BlueprintCallable)
    ECharacterMainButtonsAvailable ButtonType() const
    {
        return _buttonType;
    }

    UFUNCTION(BlueprintCallable)
    FText ButtonName() const
    {
        return _buttonName;
    }


};
