// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"


#include "GameFramework/Character.h"
#include "EscapeTheFateCharacter.generated.h"

class ABattlePlayerController;
enum class EBattleGroups : unsigned char;
class UNextBattleComponent;
class ASoundManager;
class AGameManager;
UCLASS(config=Game)
class AEscapeTheFateCharacter : public ACharacter
{
    GENERATED_BODY()

    /** Camera boom positioning the camera behind the character */
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
    class USpringArmComponent* CameraBoom;

    /** Follow camera */
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
    class UCameraComponent* FollowCamera;


    UPROPERTY(VisibleAnywhere,BlueprintReadWrite, Category = "Game Controller",meta = (AllowPrivateAccess = "true"))
    ABattlePlayerController* _mainPlayerController;
    UPROPERTY(VisibleAnywhere,BlueprintReadOnly,Category = GameStats,meta = (AllowPrivateAccess = "true"))
    UNextBattleComponent* _nextBattleComponent;

public:
    AEscapeTheFateCharacter();

    /** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
    float BaseTurnRate;

    /** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
    float BaseLookUpRate;

    UPROPERTY(EditAnywhere,BlueprintReadWrite)
    ASoundManager* soundManager;

protected:


    /** Called for forwards/backward input */
    void MoveForward(float Value);

    /** Called for side to side input */
    void MoveRight(float Value);

    /** 
     * Called via input to turn at a given rate. 
     * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
     */
    void TurnAtRate(float Rate);

    /**
     * Called via input to turn look up/down at a given rate. 
     * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
     */
    void LookUpAtRate(float Rate);

    /** Handler for when a touch input begins. */
    void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

    /** Handler for when a touch input stops. */
    void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);

protected:
    // APawn interface
    virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
    // End of APawn interface

    virtual void BeginPlay() override;

public:
    /** Returns CameraBoom subobject **/
    FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
    /** Returns FollowCamera subobject **/
    FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }

    //Public Functions
public:
    UFUNCTION(BlueprintImplementableEvent,BlueprintCallable)
    void StartBattle();

    UNextBattleComponent* NextBattleComponent() const
    {
        return _nextBattleComponent;
    }

    /**
     * @brief Called When Overlapping with the battle zones, to modify the Next Battle Stats
     */
    void SetIsInBattleZone(bool isInBattleZone_) const;
    void SetNextBattleZone(EBattleGroups nextBattleGroup_) const;
    void SetNextBattleSteps(int32 nextBattleStepCounter_);
};
