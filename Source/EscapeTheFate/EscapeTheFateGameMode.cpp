// Copyright Epic Games, Inc. All Rights Reserved.

#include "EscapeTheFateGameMode.h"
#include "EscapeTheFateCharacter.h"
#include "UObject/ConstructorHelpers.h"

AEscapeTheFateGameMode::AEscapeTheFateGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
