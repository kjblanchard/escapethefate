// Copyright - SuperGoonGames LLC 2020 - Not for use by other parties


#include "NextBattleComponent.h"
#include "BattleCharacterSpawnerComponent.h"
#include "Kismet/GameplayStatics.h"
#include "EtfGameInstance.h"


UNextBattleComponent::UNextBattleComponent() : _currentSteps(0), _battleStartSteps(9999),
                                               _isInBattleZone(false), _nextBattleGroup(EBattleGroups::DEFAULT),
                                               _players({})
{
}


// Called when the game starts
void UNextBattleComponent::BeginPlay()
{
    Super::BeginPlay();
}


void UNextBattleComponent::IncrementCurrentSteps()
{
    ++_currentSteps;
}


TArray<FCharStats> UNextBattleComponent::GetBattleCharactersToSpawnForNextBattle()
{
    return _players;
}

EBattleGroups UNextBattleComponent::GetBattleGroupForNextBattle()
{
    return _nextBattleGroup;
}

void UNextBattleComponent::SaveToGameInstance()
{
    auto EtfInstance = Cast<UEtfGameInstance>(UGameplayStatics::GetGameInstance(this));
    auto battleInfo = FNextBattleInformation{_players, _nextBattleGroup};
    EtfInstance->SetNextBattleInformation(battleInfo);
}
