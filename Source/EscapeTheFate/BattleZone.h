// Copyright - SuperGoonGames LLC 2020 - Not for use by other parties

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BattleZone.generated.h"

enum class EBattleGroups : unsigned char;
class USphereComponent;

/**
 * @brief Used In battle creation to link a battle group and its percent chance to occur together when creating the zone.
 */
USTRUCT(BlueprintType)
struct FPotentialBattleInZone
{
    GENERATED_BODY()
    UPROPERTY(EditInstanceOnly)
    EBattleGroups battleGroup;
    
    UPROPERTY(EditInstanceOnly)
    int32    weightValue;
};

UCLASS()
class ESCAPETHEFATE_API ABattleZone : public AActor
{
    GENERATED_BODY()

public:
    ABattleZone();

protected:
    virtual void BeginPlay() override;


private:
    UPROPERTY(BlueprintReadWrite,EditAnywhere,Category="Battle Zone",meta = (AllowPrivateAccess = "true"))
    USphereComponent* _sphereComponent;

    UPROPERTY(EditDefaultsOnly, Category="Spawn Zone",meta=(AllowPrivateAccess = "true"))
    TArray<FPotentialBattleInZone>  _zoneBattleGroups;

    UPROPERTY(EditDefaultsOnly, Category="Spawn Zone",meta=(AllowPrivateAccess = "true"))
    int32  _stepsUntilBattle;

    int32 _numberOfZoneGroups;
    TArray<int32> _zoneBattleWeightValues;
    int32 _highestZoneBattleWeight;

    UFUNCTION()
    void OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
                        int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
    UFUNCTION()
    void OnOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
                              UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

    UFUNCTION()
    EBattleGroups GenerateRandomBattleGroupForNextBattle();

    /**
     * @brief Used on begin game to get the proper values based on what we choose inside this battle zone.  These weights determine the next battle.
     */
    UFUNCTION()
    void GenerateBattleZoneWeights();
};
