// Copyright - SuperGoonGames LLC 2020 - Not for use by other parties


#include "BattleFullHudWidget.h"
#include "BattleTargetSelectWidget.h"
#include "BattleCommandsWidget.h"

#include "Kismet/GameplayStatics.h"

void UBattleFullHudWidget::ChangeBattleUiState(EUiWidgetStates stateToChangeTo_, EUiWidgetStates stateChangingFrom)
{
    if(_currentWidget)
    {
        _currentWidget->CloseWidget(stateToChangeTo_);
    }
    _currentWidget = _widgetDict.FindRef(stateToChangeTo_);
    if(_currentWidget)
    {
        _currentWidget->OpenWidget(stateChangingFrom);
    }

}

void UBattleFullHudWidget::NativeConstruct()
{
    auto controller =UGameplayStatics::GetPlayerController(this,0);
    if(controller)
    {
        auto inputmode = FInputModeGameAndUI();
        controller->SetInputMode(inputmode);
    }
    _widgetDict.Add(EUiWidgetStates::Main,_battleCommandsWidget);
    _widgetDict.Add(EUiWidgetStates::TargetSelection,_targetSelectWidget);
    ChangeBattleUiState(EUiWidgetStates::Main,EUiWidgetStates::DEFAULT);
    Super::NativeConstruct();
}
