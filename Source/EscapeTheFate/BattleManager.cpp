// Fill out your copyright notice in the Description page of Project Settings.


#include "BattleManager.h"
#include "BattlePlayerController.h"
#include "BattlePlayerPawn.h"
#include "EtfGameInstance.h"
#include "BtlMgrCharStatComponent.h"
#include "Kismet/GameplayStatics.h"
#include "EtfHud.h"


// Sets default values
ABattleManager::ABattleManager()
{
    _battleMgrCharStatsComponent = CreateDefaultSubobject<UBtlMgrCharStatComponent
    >(TEXT("BattleMgrCharStatsComponent"));
}

void ABattleManager::BeginPlay()
{
    Super::BeginPlay();

    auto gameInstance = GetGameInstance();
    if (gameInstance)
    {
        auto etfGameInstance = Cast<UEtfGameInstance>(gameInstance);
        if (etfGameInstance)
        {
            etfGameInstance->SetBattleManager(this);
            _etfHud = Cast<AEtfHud>(UGameplayStatics::GetPlayerController(this, 0)->GetHUD());
        }
    }
    if (_battleCharacterSpawnerComponent)
    {
        if (_battleMgrCharStatsComponent)
        {
            _battleCharacterSpawnerComponent->SetBtlMgrCharStatComponent(_battleMgrCharStatsComponent);
        }
    }
}


void ABattleManager::PossessBattlePlayer(APawn* playerPawn_)
{
    const auto playerController = Cast<ABattlePlayerController>(UGameplayStatics::GetPlayerController(this, 0));
    playerController->Possess(playerPawn_);
    auto battleplayerpawn = Cast<ABattlePlayerPawn>(playerPawn_);
    battleplayerpawn->GetBattleController();
    if (_etfHud)
    {
        battleplayerpawn->SetEtfHud(_etfHud);
    }
    else
    {
        FString name = this->GetName();
        UE_LOG(LogTemp, Warning, TEXT( " Could not get ETF hud : %s" ), *name);
    }
}

void ABattleManager::SetupBattleHud()
{
    {
        if (_etfHud)
        {
            _etfHud->SetupBattleHud(_battleMgrCharStatsComponent->PlayerBattleCharacters(),
                                    _battleMgrCharStatsComponent->EnemyBattleCharacters());
        }
        else
        {
            FString name = this->GetName();
            UE_LOG(LogTemp, Warning, TEXT( " Could not get ETF hud : %s" ), *name);
        }
    }
}

void ABattleManager::StartBattle(const TArray<FBattleGroupToTransform>& playersArray_,
                                 const TArray<FBattleGroupToTransform>& enemiesArray_,
                                 const TArray<FTransform>& cameraArray_, APawn* playerToPossess_,
                                 ACameraActor* cameraToControl_)
{
    PossessBattlePlayer(playerToPossess_);
    _battleCharacterSpawnerComponent->SpawnBattlersAndCamera(playersArray_, enemiesArray_, cameraArray_,
                                                             cameraToControl_);
    SetupBattleHud();
}

