// Copyright - SuperGoonGames LLC 2020 - Not for use by other parties


#include "BattleMenuWidgetBase.h"
#include "EtfGameInstance.h"
#include "EtfHud.h"
#include "Kismet/GameplayStatics.h"


void UBattleMenuWidgetBase::OpenWidget_Implementation(EUiWidgetStates widgetComingFrom)
{
}

void UBattleMenuWidgetBase::CloseWidget_Implementation(EUiWidgetStates widgetGoingTo)
{
}

void UBattleMenuWidgetBase::SendButtonInput_Implementation(EUiButtons buttonPressed_)
{
}

void UBattleMenuWidgetBase::ButtonPressed_Implementation(int32 buttonToSelect)
{
}

void UBattleMenuWidgetBase::UpdateAllButtons_Implementation(int32 buttonToSelect)
{
}

void UBattleMenuWidgetBase::SetAllButtonsInactive_Implementation()
{
}

void UBattleMenuWidgetBase::SetButtonActive_Implementation(int32 buttonToSelect)
{
}

void UBattleMenuWidgetBase::PlaySfx(SfxEnum soundToPlay)
{
    if (!_soundManager)
    {
        auto instance = UGameplayStatics::GetGameInstance(this);
        if (instance)
        {
            auto etfInstance = Cast<UEtfGameInstance>(instance);
            if (etfInstance)
            {
                _soundManager = etfInstance->SoundManager();
            }
        }
    }
    if (_soundManager)
    {
        _soundManager->PlaySfx(soundToPlay);
    }
}

UBattleFullHudWidget* UBattleMenuWidgetBase::GetFullBattleHudReference()
{
    auto controller = UGameplayStatics::GetPlayerController(this, 0);
    if (controller)
    {
        auto hud = controller->GetHUD();
        if (hud)
        {
            auto etfHud = Cast<AEtfHud>(hud);
            if (etfHud)
            {
                return etfHud->_fullBattleHud;
            }
        }
    }
    return nullptr;
}
