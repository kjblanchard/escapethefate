// Copyright - SuperGoonGames LLC 2020 - Not for use by other parties

#pragma once

#include "CoreMinimal.h"
#include "BattleMenuWidgetBase.h"
#include "UObject/ObjectMacros.h"

#include "BattleTargetSelectWidget.generated.h"

/**
 * 
 */
UCLASS()
class ESCAPETHEFATE_API UBattleTargetSelectWidget : public UBattleMenuWidgetBase
{
    GENERATED_BODY()

    //private variables
    UPROPERTY(BlueprintReadOnly,meta= (BindWidget, AllowPrivateAccess = "true"))
    bool _isSelectingEnemies;
    UPROPERTY(BlueprintReadOnly,meta= (BindWidget, AllowPrivateAccess = "true"))
    bool _isSelectingPlayers;

    //private methods
    UFUNCTION(BlueprintCallable)
    void SwitchToEnemies();
    UFUNCTION(BlueprintCallable)
    void SwitchToPlayers();


    //public methods to override
public:
    virtual void NativeConstruct() override;
};
