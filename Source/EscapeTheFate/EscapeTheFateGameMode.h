// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "EscapeTheFateGameMode.generated.h"

UCLASS(minimalapi)
class AEscapeTheFateGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AEscapeTheFateGameMode();
};



