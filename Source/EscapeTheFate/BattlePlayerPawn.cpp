// Copyright - SuperGoonGames LLC 2020 - Not for use by other parties


#include "BattlePlayerPawn.h"
#include "Blueprint/UserWidget.h"
#include "BattlePlayerController.h"
#include "EtfHud.h"


void ABattlePlayerPawn::UpButton()
{
    _etfHud->SendButtonInput(EUiButtons::Up);
}

void ABattlePlayerPawn::DownButton()
{
    _etfHud->SendButtonInput(EUiButtons::Down);
}

void ABattlePlayerPawn::SelectButton()
{
    _etfHud->SendButtonInput(EUiButtons::Confirm);
}

void ABattlePlayerPawn::CancelButton()
{
    _etfHud->SendButtonInput(EUiButtons::Cancel);
}

ABattlePlayerPawn::ABattlePlayerPawn()
{
}

void ABattlePlayerPawn::BeginPlay()
{
    Super::BeginPlay();
    GetBattleController();
}



// Called to bind functionality to input
void ABattlePlayerPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
    Super::SetupPlayerInputComponent(PlayerInputComponent);
    PlayerInputComponent->BindAction("Select", IE_Pressed, this, &ABattlePlayerPawn::SelectButton);
    PlayerInputComponent->BindAction("UI_up", IE_Pressed, this, &ABattlePlayerPawn::UpButton);
    PlayerInputComponent->BindAction("UI_down", IE_Pressed, this, &ABattlePlayerPawn::DownButton);
    PlayerInputComponent->BindAction("UI_cancel", IE_Pressed,this,&ABattlePlayerPawn::CancelButton);
}

void ABattlePlayerPawn::GetBattleController()
{
    battlePlayerController = Cast<ABattlePlayerController>(GetController());
}
