// Copyright - SuperGoonGames LLC 2020 - Not for use by other parties

#pragma once

#include "CoreMinimal.h"

#include "BattleMenuWidgetBase.h"

#include "BattleFullHudWidget.generated.h"

class UBattleTargetSelectWidget;
class UBattleCommandsWidget;
/**
 * 
 */
UCLASS()
class ESCAPETHEFATE_API UBattleFullHudWidget : public UBattleMenuWidgetBase
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadWrite,meta= (BindWidget, AllowPrivateAccess = "true"))
	TMap<EUiWidgetStates,UBattleMenuWidgetBase*>_widgetDict;
	
	UPROPERTY(BlueprintReadWrite,meta= (AllowPrivateAccess = "true"))
	UBattleMenuWidgetBase* _currentWidget;

	UPROPERTY(BlueprintReadWrite,meta= (BindWidget, AllowPrivateAccess = "true"))
	UBattleCommandsWidget* _battleCommandsWidget;

	UPROPERTY(BlueprintReadWrite,meta= (BindWidget, AllowPrivateAccess = "true"))
	UBattleTargetSelectWidget* _targetSelectWidget;

	
	


	public:
	UFUNCTION( BlueprintCallable)
	void ChangeBattleUiState(EUiWidgetStates stateToChangeTo_, EUiWidgetStates stateChangingFrom);

	virtual void NativeConstruct() override;
};
