// Copyright - SuperGoonGames LLC 2020 - Not for use by other parties


#include "BattlePlayerController.h"


void ABattlePlayerController::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);
}

void ABattlePlayerController::BeginPlay()
{
    Super::BeginPlay();
}

// #if WITH_EDITOR
// /* Get Screen Percentage */
// static const auto CVarScreenPercentage = IConsoleManager::Get().FindTConsoleVariableDataFloat(
//     TEXT("r.SCreenPercentage"));
// #endif WITH_EDITOR
// float ABattlePlayerController::GetObjectScreenRadius(AActor* inActor_)
// {
//     float ScreenRadius;
//     int32 Width, Height;
//     FVector Viewlocation;
//     FRotator ViewRotation; // Not Used, but required for Function call
//     float CamFOV = 90.0f; //TODO: Replace With Function that returns camera FOV
// #if WITH_EDITOR
//     float ScreenPerc = CVarScreenPercentage->GetValueOnGameThread() / 100.0f;
// #endif WITH_EDITOR
//
//     /* Get the size of the viewport, and the player cameras location. */
//     GetViewportSize(Width, Height);
//     GetPlayerViewPoint(Viewlocation, ViewRotation);
//
// #if WITH_EDITOR
//     /* Factor in Screen Percentage & Quality Settings */
//     Width *= ScreenPerc;
//     Height *= ScreenPerc;
// #endif WITH_EDITOR
//
//     /* Easy Way To Return The Size, Create a vector and scale it. Alternative would be to use FMath::Max3 */
//     float SRad = FVector2D(Width, Height).Size();
//
//     /* Get Object Bounds (R) */
//     float BoundingRadius = inActor_->GetRootComponent()->Bounds.SphereRadius;
//     float DistanceToObject = FVector(inActor_->GetActorLocation() - Viewlocation).Size();
//
//     /* Get Projected Screen Radius */
//     ScreenRadius = FMath::Atan(BoundingRadius / DistanceToObject);
//     ScreenRadius *= SRad / FMath::DegreesToRadians(CamFOV);
//
//     return ScreenRadius;
// }
