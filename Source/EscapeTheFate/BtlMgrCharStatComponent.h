// Copyright - SuperGoonGames LLC 2020 - Not for use by other parties

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "BtlMgrCharStatComponent.generated.h"


class ABattleCharacter;
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ESCAPETHEFATE_API UBtlMgrCharStatComponent : public UActorComponent
{
    GENERATED_BODY()

    UPROPERTY(VisibleAnywhere)
    TArray<ABattleCharacter*> _playerBattleCharacters;

    UPROPERTY(VisibleAnywhere)
    TArray<ABattleCharacter*> _enemyBattleCharacters;


protected:
    virtual void BeginPlay() override;


public:

    UBtlMgrCharStatComponent();

    /**
     * @brief This is called when the battle starts to add the character pointers to the battle.
     * @param playerToAdd_ The player that is spawned into the field already, that should be added to the players list
     */
    void AddToPlayersList(ABattleCharacter* playerToAdd_);

    /**
     * @brief This is called when the battle starts to add the character pointers to the stats table.
     * @param enemyToAdd_ The enemy that is spawned into the level already, that should be added to the enemies list.
     */
    void AddToEnemiesList(ABattleCharacter* enemyToAdd_);


    /**
     * @brief Testing purposes 11/1/20 for basic damage to the enemy.
     * @param danageGiven Damage
     * @param enemyToAttack Enemy to damage
     */
    UFUNCTION(BlueprintCallable)
    void DecrementHealth(int danageGiven, int enemyToAttack);


    //Getters and setters
    UFUNCTION(BlueprintCallable)
    TArray<ABattleCharacter*> PlayerBattleCharacters() const
    {
        return _playerBattleCharacters;
    }

    UFUNCTION(BlueprintCallable)
    TArray<ABattleCharacter*> EnemyBattleCharacters() const
    {
        return _enemyBattleCharacters;
    }
};
