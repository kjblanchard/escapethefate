// Copyright Epic Games, Inc. All Rights Reserved.

using System.IO.IsolatedStorage;
using UnrealBuildTool;

public class EscapeTheFate : ModuleRules
{
	public EscapeTheFate(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay","UMG", "UMGEditor" });
		
		PrivateDependencyModuleNames.AddRange(new string[] { "Slate", "SlateCore" });

	}
}
