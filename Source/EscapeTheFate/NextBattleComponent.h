// Copyright - SuperGoonGames LLC 2020 - Not for use by other parties

#pragma once

#include "Components/ActorComponent.h"
#include "CoreMinimal.h"


#include "BattleCharacterSpawnerComponent.h"
#include "ISaveInformationToInstance.h"

#include "NextBattleComponent.generated.h"


enum class EBattleCharacterToSpawn : unsigned char;
class ABattleCharacter;
enum class EBattleGroups : unsigned char;
class AEscapeTheFateCharacter;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ESCAPETHEFATE_API UNextBattleComponent : public UActorComponent, public ISaveInformationToInstance
{
    GENERATED_BODY()

    UNextBattleComponent();


protected:
    virtual void BeginPlay() override;

    //Private member variables
private:
    UPROPERTY()
    int32 _currentSteps;

    UPROPERTY()
    int32 _battleStartSteps;

    UPROPERTY(VisibleInstanceOnly)
    bool _isInBattleZone;

    UPROPERTY(VisibleInstanceOnly)
    EBattleGroups _nextBattleGroup;

    UPROPERTY(EditAnywhere,Category="Battle Actors | Players",meta = (AllowPrivateAccess = "true")
    )
    TArray<FCharStats> _players;


    //Public Functions
public:

    UFUNCTION(BlueprintCallable)
    void IncrementCurrentSteps();


    UFUNCTION(BlueprintCallable)
    void ResetCurrentSteps()
    {
        _currentSteps = 0;
    }

    //Public Member Variable Getters/Setters

    UFUNCTION(BlueprintCallable)
    EBattleGroups NextBattleGroup() const
    {
        return _nextBattleGroup;
    }

    UFUNCTION(BlueprintCallable)
    bool GetIsInBattleZone() const
    {
        return _isInBattleZone;
    }

    void SetIsInBattleZone(const bool isInBattleZone_)
    {
        _isInBattleZone = isInBattleZone_;
    }

    void SetNextBattleGroup(EBattleGroups nextBattleGroup_)
    {
        _nextBattleGroup = nextBattleGroup_;
    }

    UFUNCTION(BlueprintCallable)
    int32 GetBattleStartSteps() const
    {
        return _battleStartSteps;
    }

    void SetBattleStartSteps(int32 battleSteps_)
    {
        _battleStartSteps = battleSteps_;
    }

    UFUNCTION(BlueprintCallable)
    int32 GetCurrentSteps() const
    {
        return _currentSteps;
    }

    UFUNCTION(BlueprintCallable)
    TArray<FCharStats> GetBattleCharactersToSpawnForNextBattle();

    UFUNCTION(BlueprintCallable)
    EBattleGroups GetBattleGroupForNextBattle();

    UFUNCTION(BlueprintCallable)
    virtual void SaveToGameInstance() override;
};
